/**
 * Shlomo Bensimhon
 * 1837702
 *
 * 12/09/2021
 *
 * This class is used to calculate BMI (Body Mass Index)
 * given a weight and height from the user
 *
 */


package com.shlomo.bmicalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

/**
 * This Class gets elements from activity_main.xml and uses
 * them to calculate BMI. activity_main.xml has user input
 * as well textviews needed to display the BMI calculated.
 *
 *
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var temptext = findViewById<TextView>(R.id.ResultText)
        var button = findViewById<Button>(R.id.ConvertButton)
        var massInput = findViewById<TextView>(R.id.MassInput);
        var heightInput = findViewById<TextView>(R.id.HeightInput);
        var healthPhrase = findViewById<TextView>(R.id.HealthPhrase);

        button.setOnClickListener {
            if (massInput.text.toString() == "" || heightInput.text.toString() == "") {
                temptext.text = "Mass or Height is missing"
            } else {
                var massDouble = massInput.text.toString().toDouble();
                var heightDouble = heightInput.text.toString().toDouble();

                if (massDouble > 300 || massDouble < 10) {
                    temptext.text = "Invalid mass";
                }else if(heightDouble > 2.2 || heightDouble < 0.2){
                    temptext.text = "Invalid Height";
                }else{
                    var temp = ((massDouble / heightDouble) / heightDouble);
                    temptext.text = temp.toString();

                    /**
                     * These phrases are http://en.wikipedia.org/wiki/Body_mass_index and
                     * indicate levels of risk in accordance with BMI in singapore.
                     *
                     */
                    if(temp < 18.5){
                        healthPhrase.text = "Possible nutritional deficiency and osteoporosis.";
                    }else if(temp > 18.5 && temp < 22.9){
                        healthPhrase.text = "Low risk (healthy range).";
                    }else if(temp > 23.0 && temp < 27.4){
                        healthPhrase.text = "Moderate risk of developing heart disease, high blood pressure, stroke, diabetes mellitus. ";
                    }else{
                        healthPhrase.text = "High risk of developing heart disease, high blood pressure, stroke, diabetes mellitus. Metabolic Syndrome.";
                    }
                }
            }
        }
    }
}


